#version 400 core

// The fog parameters
struct Fog {
    vec3 color;
    float maxEffect;
    float start;
    float end;
};

// Calculates the linear fog effect (you can add functions for other fog types)
float calculateLinearFogEffect(Fog fog, float distance) {
    return clamp((distance - fog.start) / (fog.end - fog.start), 0.0, 1.0) * fog.maxEffect;
}
