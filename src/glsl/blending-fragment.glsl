#version 400 core

struct Fog {
    vec3 color;
    float maxEffect;
    float start;
    float end;
};

float calculateLinearFogEffect(Fog fog, float distance);

uniform sampler2D u_texture1; // The first texture
uniform sampler2D u_texture2; // The second tetxure
uniform float u_blendingFactor; // The blending factor (the opacity of the first texture)
uniform Fog u_fog; // The fog parameters

in vec2 v_textureCoordinates; // The texture coordinates
in float v_distance; // The distance from the camera to the fragment

out vec4 color; // The fragment color

void main() {
    vec4 t1 = texture(u_texture1, v_textureCoordinates); // The color from the first texture
    vec4 t2 = texture(u_texture2, v_textureCoordinates); // The color from the second texture
    float fogEffect = calculateLinearFogEffect(u_fog, v_distance); // Calculate the linear fog effect
    color = mix(mix(t2, t1, u_blendingFactor), vec4(u_fog.color, 1.0), fogEffect);  // Set the fragment color based on the blended color and the fog effect
}
