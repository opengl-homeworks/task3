#version 400 core

layout (location = 0) in vec3 a_position; // The vertex coordinates
layout (location = 1) in vec2 a_textureCoordinates; // The texture coordinates

uniform mat4 u_modelMatrix; // The model matrix
uniform mat4 u_matrix; // The transformation matrix
uniform vec3 u_cameraPosition; // The camera position

out vec2 v_textureCoordinates; // The texture coordinates
out float v_distance; // The distance from the camera to the vertex

void main() {
    v_textureCoordinates = a_textureCoordinates; // Pass the texture coordinates
    v_distance = distance(u_cameraPosition, vec3(u_modelMatrix * vec4(a_position, 1.0))); // Calculate the distance from the camera to the vertex
    gl_Position = u_matrix * vec4(a_position, 1.0); // Calculate the vertex position in the clip space
}
