#version 400 core

struct Fog {
    vec3 color;
    float maxEffect;
    float start;
    float end;
};

uniform samplerCube u_texture; // The skybox texture
uniform Fog u_fog; // The fog parameters

in vec3 v_textureCoordinates; // The texture coordinates

out vec4 color; // The fragment color

void main() {
    color = mix(texture(u_texture, v_textureCoordinates), vec4(u_fog.color, 1.0), u_fog.maxEffect); // Set the fragment color based on the skybox texture and the fog effect
}
