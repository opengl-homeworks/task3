#version 400 core

struct Fog {
    vec3 color;
    float maxEffect;
    float start;
    float end;
};

float calculateLinearFogEffect(Fog fog, float distance);

uniform sampler2D u_texture; // The texture
uniform Fog u_fog; // The fog parameters

in vec2 v_textureCoordinates; // The texture coordinates
in float v_lighting; // The lighting
in float v_distance; // The distance from the camera to the fragment

out vec4 color; // The fragment color

void main() {
    float fogEffect = calculateLinearFogEffect(u_fog, v_distance); // Calculate the linear fog effect
    color = mix(texture(u_texture, v_textureCoordinates) * v_lighting, vec4(u_fog.color, 1.0), fogEffect); // Set the fragment color based on the texture, lighting and the fog effect
}
