#pragma once

#include <vector>

/**
 * The loaded mesh data
 */
class MeshData {

public:

    /**
     * The vertices count of the mesh
     */
    int verticesCount;

    /**
     * The image index in the images array
     */
    int imageIndex;

public:

    /**
     * Constructor
     * @param verticesCount the vertices count in the mesh
     * @param imageIndex the image index in the images array
     */
    MeshData(int verticesCount, int imageIndex);

};
