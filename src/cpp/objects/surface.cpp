#include "surface.h"

#include <random>
#include <vector>

#include "../loaders/image/load_image.h"

const float OFFSET = 500;
const float SIZE = OFFSET * 2;
// The texture repeat count
const float N_TEX = 12;
const int N = 50;
const float STEP = SIZE / N;
const float TEX_STEP = STEP * N_TEX / SIZE;

Surface::Surface(std::string image1Path, std::string image2Path) : image1Path(std::move(image1Path)), image2Path(std::move(image2Path)) {}

void Surface::initialize() {
    // C++ 11 style random
    std::mt19937 mt(1);
    std::uniform_real_distribution<float> dist(0.0, 1.0);
    auto rand = [&dist, &mt]() { return dist(mt); };

    std::vector<float> vertices;

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            float offsetX = (rand() - 0.5f) / 3;
            float offsetY = (rand() - 0.5f) / 3;

            // The coordinates (x, y, z)
            vertices.push_back(-OFFSET + STEP * (i + offsetX));
            vertices.push_back(rand() * 10 - 5);

            // The texture coordinates (s, t)
            vertices.push_back(-OFFSET + STEP * (j + offsetY));
            vertices.push_back(TEX_STEP * (i + offsetX));
            vertices.push_back(TEX_STEP * (j + offsetY));
        }
    }

    std::vector<int> indices;

    for (int i = 0; i < N - 1; i++) {
        for (int j = 0; j < N - 1; j++) {
            indices.push_back(i * N + j);
            indices.push_back(i * N + j + 1);
            indices.push_back((i + 1) * N + j);
            indices.push_back((i + 1) * N + j);
            indices.push_back(i * N + j + 1);
            indices.push_back((i + 1) * N + j + 1);
        }
    }

    verticesCount = indices.size();

    std::vector<GLfloat> data(indices.size() * 5);

    for (int i = 0; i < indices.size(); i++) {
        data[i * 5] = vertices[indices[i] * 5];
        data[i * 5 + 1] = vertices[indices[i] * 5 + 1];
        data[i * 5 + 2] = vertices[indices[i] * 5 + 2];
        data[i * 5 + 3] = vertices[indices[i] * 5 + 3];
        data[i * 5 + 4] = vertices[indices[i] * 5 + 4];
    }

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(GLfloat), data.data(), GL_STATIC_DRAW);

    // The a_position attribute
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*) 0);

    // The a_textureCoordinates attribute
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*) (3 * sizeof(GLfloat)));

    Image image1 = loadImage(image1Path, true);
    Image image2 = loadImage(image2Path, true);

    glGenTextures(1, &texture1);
    glBindTexture(GL_TEXTURE_2D, texture1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image1.width, image1.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image1.bytes.data());

    glGenTextures(1, &texture2);
    glBindTexture(GL_TEXTURE_2D, texture2);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image2.width, image2.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image2.bytes.data());
}

void Surface::draw() const {
    glBindVertexArray(vao);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture1);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture2);

    glDrawArrays(GL_TRIANGLES, 0, verticesCount);
}
