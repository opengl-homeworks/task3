#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

#include <string>

#include "object3d.h"

/**
 * The surface with two blended textures
 */
class Surface : public Object3D {

public:

    /**
     * The path to the first texture image
     */
    std::string image1Path;

    /**
     * The path to the second texture image
     */
    std::string image2Path;

    /**
     * The first texture
     */
    GLuint texture1 = 0;

    /**
     * The second texture
     */
    GLuint texture2 = 0;

    /**
     * The vertices count
     */
    int verticesCount = 0;

public:

    /**
     * Constructor
     * @param image1Path the path to the first texture image
     * @param image2Path the path to the second texture image
     */
    Surface(std::string image1Path, std::string image2Path);

    void initialize() override;

    void draw() const override;
};
