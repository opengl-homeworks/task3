#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

#include <string>

#include "object3d.h"

/**
 * The textured planet
 */
class Planet : public Object3D {

public:

    /**
     * The path to the texture image
     */
    std::string imagePath;

    /**
     * The texture
     */
    GLuint texture = 0;

    /**
     * The count of the vertex indices
     */
    int indicesCount = 0;

public:

    /**
     * Constructor
     * @param imagePath the path to the texture image
     */
    explicit Planet(std::string imagePath);

    /**
     * Constructor
     * @param imagePath the path to the texture image
     * @param position the planet position
     */
    Planet(std::string imagePath, glm::vec3 position);

    void initialize() override;

    void update(float dt) override;

    void draw() const override;

};
