#define GLFW_INCLUDE_NONE

#include <GLFW/glfw3.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <iostream>

#include "loaders/text/load_text.h"
#include "utils/opengl.h"
#include "camera.h"
#include "objects/planet.h"
#include "objects/surface.h"
#include "objects/textured_model.h"
#include "objects/skybox.h"

const float PI = glm::pi<float>();

// The field of view
const float FOV_Y = 70;

const float CAMERA_ROTATION_SPEED = 60;

const float CAMERA_MOVEMENT_SPEED_LOW = 6;
const float CAMERA_MOVEMENT_SPEED_HIGH = 60;

float cameraMovementSpeed = CAMERA_MOVEMENT_SPEED_LOW;

const glm::vec3 FOG_COLOR(0.8, 0.8, 0.8);

float fogMaxEffect = 1;

const float FOG_START = 1;

const float FOG_END = 100;

GLFWwindow* window;

// The viewport width
int width = 800;

// The viewport height
int height = 640;

// The viewport aspect
float aspect = (float) width / (float) height;

// The mouse x coordinate
double mouseX = 0;

// The mouse y coordinate
double mouseY = 0;

// Shows whether the mouse is captured
bool mouseCaptured;

// The elapsed time since the GLFW initialization in seconds
float t;

// The elapsed time since the last frame in seconds
float dt;

struct FogUniform {
    GLuint color;
    GLuint maxEffect;
    GLuint start;
    GLuint end;
};

// The OpenGL variables for the skybox program
GLuint skyboxProgram;
struct {
    GLuint u_forward;
    GLuint u_clipPlaneRight;
    GLuint u_clipPlaneUp;
    GLuint u_texture;
    FogUniform u_fog;
} skyboxUniforms;

// The OpenGL variables for the texture program
GLuint textureProgram;
struct {
    GLuint u_lightDirection;
    GLuint u_modelMatrix;
    GLuint u_normalMatrix;
    GLuint u_matrix;
    GLuint u_cameraPosition;
    GLuint u_texture;
    FogUniform u_fog;
} textureUniforms;

// The OpenGL variables for the blending program
GLuint blendingProgram;
struct {
    GLuint u_modelMatrix;
    GLuint u_matrix;
    GLuint u_cameraPosition;
    GLuint u_texture1;
    GLuint u_texture2;
    GLfloat u_blendingFactor;
    FogUniform u_fog;
} blendingUniforms;

Camera camera(0, 2, 20);

glm::mat4 projectionViewMatrix;

Skybox skybox("../res/image/skybox/right.png",
              "../res/image/skybox/left.png",
              "../res/image/skybox/up.png",
              "../res/image/skybox/down.png",
              "../res/image/skybox/front.png",
              "../res/image/skybox/back.png");

Planet planet("../res/image/mars.png");

TexturedModel starship("../res/model/starship/Wraith Raider Starship.obj");

TexturedModel module[] = {
        TexturedModel("../res/model/HDU/HDU_lowRez_part1.obj"),
        TexturedModel("../res/model/HDU/HDU_lowRez_part2.obj")
};

Surface surface("../res/image/moon.png", "../res/image/snow.png");

GLfloat surfaceBlendingFactor = 1;

void initializeScene();

void initializeSkyboxProgram();

void initializeTextureProgram(GLuint fogShader);

void initializeBlendingProgram(GLuint fogShader);

void resizeViewport();

void update();

void updateControls();

void updateProjectionViewMatrix();

void draw();

void glfwErrorCallback(int error, const char* description) {
    std::cerr << "The GLFW function call completed with errors:\n" << description << std::endl;
}

void glfwCursorPosCallback(GLFWwindow* window, double xpos, double ypos) {
    if (mouseCaptured) {
        camera.rotateHorizontally((xpos - mouseX) * glm::radians(0.1f));
        camera.rotateVertically(-(ypos - mouseY) * glm::radians(0.1f));

        mouseX = xpos;
        mouseY = ypos;
    }
}

void glfwMouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        mouseCaptured = true;
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        glfwGetCursorPos(window, &mouseX, &mouseY);
    }
}

void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        mouseCaptured = false;
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
    else if (key == GLFW_KEY_LEFT_SHIFT && action == GLFW_PRESS) {
        if (cameraMovementSpeed == CAMERA_MOVEMENT_SPEED_LOW) cameraMovementSpeed = CAMERA_MOVEMENT_SPEED_HIGH;
        else cameraMovementSpeed = CAMERA_MOVEMENT_SPEED_LOW;
    }
}

int main() {
    glfwSetErrorCallback(glfwErrorCallback);

    if (!glfwInit()) return -1;

    // Set the OpenGL version window hints
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    // Set the OpenGL context window hints (required for macOS)
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(width, height, "Task 3", nullptr, nullptr);

    // Close the application if the window isn't created
    if (!window) {
        glfwTerminate();
        return -1;
    }

    // Set the callbacks for controls
    glfwSetCursorPosCallback(window, glfwCursorPosCallback);
    glfwSetMouseButtonCallback(window, glfwMouseButtonCallback);
    glfwSetKeyCallback(window, glfwKeyCallback);

    // Enable the raw mouse motion for the camera control
    if (glfwRawMouseMotionSupported()) glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
    else std::cout << "Raw mouse motion isn't supported" << std::endl;

    glfwMakeContextCurrent(window);

    // Limit the frame rate to a screen refresh rate
    glfwSwapInterval(1);

    GLenum glewStatus = glewInit();

    if (glewStatus != GLEW_OK) {
        std::cerr << "The GLEW initialization completed with errors:\n" << glewGetErrorString(glewStatus) << std::endl;
        glfwTerminate();
        return -1;
    }

    try {
        initializeScene();
    }
    catch (const std::exception &e) {
        // Close the application if there is an error while loading the files, compiling the shaders or linking the program
        std::cerr << e.what() << std::endl;
        glfwTerminate();
        return -1;
    }

    // Enable GL_CULL_FACE to draw only the visible faces
    glEnable(GL_CULL_FACE);

    // Get the initial time
    t = glfwGetTime();

    while (!glfwWindowShouldClose(window)) {
        // Don't update and draw the scene if the program window is minimized
        if (glfwGetWindowAttrib(window, GLFW_ICONIFIED)) {
            glfwWaitEvents();
            continue;
        }

        // Calculate the elapsed time
        float nt = glfwGetTime();
        dt = nt - t;
        t = nt;


        resizeViewport();
        update();
        draw();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}

void initializeScene() {
    std::string fogShaderSource = loadText("../src/glsl/fog.glsl");

    GLuint fogShader = createShader(fogShaderSource, GL_FRAGMENT_SHADER);

    initializeSkyboxProgram();
    initializeTextureProgram(fogShader);
    initializeBlendingProgram(fogShader);

    skybox.initialize();

    starship.initialize();

    starship.position = glm::vec3(0, -1, 0);
    starship.rotation = glm::vec3(0.1, 0, -0.2);
    starship.scale = glm::vec3(0.05, 0.05, 0.05);

    for (TexturedModel& part : module) {
        part.rotation = glm::vec3(0, PI / 2, 0);
        part.position = glm::vec3(80, -1, -10);
        part.scale = glm::vec3(0.05, 0.05, 0.05);

        part.initialize();
    }

    planet.initialize();

    planet.position = glm::vec3(0, 100, -100);
    planet.scale = glm::vec3(50.0, 50.0, 50.0);

    surface.initialize();
}

void initializeSkyboxProgram() {
    std::string vertexShaderSource = loadText("../src/glsl/skybox-vertex.glsl");
    std::string fragmentShaderSource = loadText("../src/glsl/skybox-fragment.glsl");

    GLuint vertexShader = createShader(vertexShaderSource, GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader(fragmentShaderSource, GL_FRAGMENT_SHADER);

    skyboxProgram = createProgram(vertexShader, fragmentShader);

    skyboxUniforms.u_forward = glGetUniformLocation(skyboxProgram, "u_forward");
    skyboxUniforms.u_clipPlaneRight = glGetUniformLocation(skyboxProgram, "u_clipPlaneRight");
    skyboxUniforms.u_clipPlaneUp = glGetUniformLocation(skyboxProgram, "u_clipPlaneUp");
    skyboxUniforms.u_texture = glGetUniformLocation(skyboxProgram, "u_texture");

    skyboxUniforms.u_fog.color = glGetUniformLocation(skyboxProgram, "u_fog.color");
    skyboxUniforms.u_fog.maxEffect = glGetUniformLocation(skyboxProgram, "u_fog.maxEffect");
    skyboxUniforms.u_fog.start = glGetUniformLocation(skyboxProgram, "u_fog.start");
    skyboxUniforms.u_fog.end = glGetUniformLocation(skyboxProgram, "u_fog.end");

    glUseProgram(skyboxProgram);

    glUniform1i(skyboxUniforms.u_texture, 0);
}

void initializeTextureProgram(GLuint fogShader) {
    std::string vertexShaderSource = loadText("../src/glsl/texture-vertex.glsl");
    std::string fragmentShaderSource = loadText("../src/glsl/texture-fragment.glsl");

    GLuint vertexShader = createShader(vertexShaderSource, GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader(fragmentShaderSource, GL_FRAGMENT_SHADER);

    textureProgram = createProgram({ vertexShader, fragmentShader, fogShader });

    textureUniforms.u_lightDirection = glGetUniformLocation(textureProgram, "u_lightDirection");
    textureUniforms.u_modelMatrix = glGetUniformLocation(textureProgram, "u_modelMatrix");
    textureUniforms.u_normalMatrix = glGetUniformLocation(textureProgram, "u_normalMatrix");
    textureUniforms.u_matrix = glGetUniformLocation(textureProgram, "u_matrix");
    textureUniforms.u_cameraPosition = glGetUniformLocation(textureProgram, "u_cameraPosition");
    textureUniforms.u_texture = glGetUniformLocation(textureProgram, "u_texture");

    textureUniforms.u_fog.color = glGetUniformLocation(textureProgram, "u_fog.color");
    textureUniforms.u_fog.maxEffect = glGetUniformLocation(textureProgram, "u_fog.maxEffect");
    textureUniforms.u_fog.start = glGetUniformLocation(textureProgram, "u_fog.start");
    textureUniforms.u_fog.end = glGetUniformLocation(textureProgram, "u_fog.end");

    glUseProgram(textureProgram);

    glUniform1i(textureUniforms.u_texture, 0);
}

void initializeBlendingProgram(GLuint fogShader) {
    std::string vertexShaderSource = loadText("../src/glsl/blending-vertex.glsl");
    std::string fragmentShaderSource = loadText("../src/glsl/blending-fragment.glsl");

    GLuint vertexShader = createShader(vertexShaderSource, GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader(fragmentShaderSource, GL_FRAGMENT_SHADER);

    blendingProgram = createProgram({ vertexShader, fragmentShader, fogShader });

    blendingUniforms.u_blendingFactor = glGetUniformLocation(blendingProgram, "u_blendingFactor");
    blendingUniforms.u_modelMatrix = glGetUniformLocation(blendingProgram, "u_modelMatrix");
    blendingUniforms.u_matrix = glGetUniformLocation(blendingProgram, "u_matrix");
    blendingUniforms.u_cameraPosition = glGetUniformLocation(blendingProgram, "u_cameraPosition");
    blendingUniforms.u_texture1 = glGetUniformLocation(blendingProgram, "u_texture1");
    blendingUniforms.u_texture2 = glGetUniformLocation(blendingProgram, "u_texture2");

    blendingUniforms.u_fog.color = glGetUniformLocation(blendingProgram, "u_fog.color");
    blendingUniforms.u_fog.maxEffect = glGetUniformLocation(blendingProgram, "u_fog.maxEffect");
    blendingUniforms.u_fog.start = glGetUniformLocation(blendingProgram, "u_fog.start");
    blendingUniforms.u_fog.end = glGetUniformLocation(blendingProgram, "u_fog.end");

    glUseProgram(blendingProgram);

    glUniform1i(blendingUniforms.u_texture1, 0);
    glUniform1i(blendingUniforms.u_texture2, 1);
}

void resizeViewport() {
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);
    aspect = (float) width / (float) height;
}

void update() {
    updateControls();
    updateProjectionViewMatrix();

    planet.update(dt);
}

void updateControls() {
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) camera.rotateHorizontally(-glm::radians(CAMERA_ROTATION_SPEED) * dt);
    else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) camera.rotateHorizontally(glm::radians(CAMERA_ROTATION_SPEED) * dt);

    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) camera.rotateVertically(-glm::radians(CAMERA_ROTATION_SPEED) * dt);
    else if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) camera.rotateVertically(glm::radians(CAMERA_ROTATION_SPEED) * dt);

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) camera.position -= camera.right * cameraMovementSpeed * dt;
    else if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) camera.position += camera.right * cameraMovementSpeed * dt;

    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) camera.position -= camera.forward * cameraMovementSpeed * dt;
    else if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) camera.position += camera.forward * cameraMovementSpeed * dt;

    if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS) camera.position.y -= cameraMovementSpeed * dt;
    else if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) camera.position.y += cameraMovementSpeed * dt;

    camera.updateDirection();

    if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) surfaceBlendingFactor = std::min(surfaceBlendingFactor + dt, 1.0f);
    else if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) surfaceBlendingFactor = std::max(surfaceBlendingFactor - dt, 0.0f);

    if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS) fogMaxEffect = std::max(fogMaxEffect - dt, 0.0f);
    else if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS) fogMaxEffect = std::min(fogMaxEffect + dt, 1.0f);
}

void updateProjectionViewMatrix() {
    glm::mat4 projectionMatrix = glm::perspective(glm::radians(FOV_Y), aspect, 0.1f, 500.0f);
    glm::vec3 center = camera.position + camera.forward;
    glm::mat4 viewMatrix = glm::lookAt(camera.position, center, camera.up);
    projectionViewMatrix = projectionMatrix * viewMatrix;
}

void draw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(skyboxProgram);

    // Draw the skybox
    glUniform3fv(skyboxUniforms.u_forward, 1, (GLfloat*) &camera.forward);

    float tanY = std::tan(glm::radians(FOV_Y / 2));
    float tanX = tanY * aspect;

    glm::vec3 clipPlaneRight = tanX * camera.right;
    glm::vec3 clipPlaneUp = tanY * camera.up;

    glUniform3fv(skyboxUniforms.u_clipPlaneRight, 1, (GLfloat*) &clipPlaneRight);
    glUniform3fv(skyboxUniforms.u_clipPlaneUp, 1, (GLfloat*) &clipPlaneUp);

    glUniform3fv(skyboxUniforms.u_fog.color, 1, (GLfloat*) &FOG_COLOR);
    glUniform1f(skyboxUniforms.u_fog.maxEffect, fogMaxEffect);
    glUniform1f(skyboxUniforms.u_fog.start, FOG_START);
    glUniform1f(skyboxUniforms.u_fog.end, FOG_END);

    skybox.draw();

    glEnable(GL_DEPTH_TEST);

    glm::mat4 modelMatrix;
    glm::mat4 normalMatrix;
    glm::mat4 matrix;

    glUseProgram(textureProgram);

    glm::vec3 lightDirection(0.0, -10.0, -10.0);

    glUniform3fv(textureUniforms.u_lightDirection, 1, (GLfloat*) &lightDirection);

    glUniform3fv(textureUniforms.u_cameraPosition, 1, (GLfloat*) &camera.position);

    glUniform3fv(textureUniforms.u_fog.color, 1, (GLfloat*) &FOG_COLOR);
    glUniform1f(textureUniforms.u_fog.maxEffect, fogMaxEffect);
    glUniform1f(textureUniforms.u_fog.start, FOG_START);
    glUniform1f(textureUniforms.u_fog.end, FOG_END);

    // Draw the starship
    modelMatrix = starship.getModelMatrix();
    normalMatrix = starship.getNormalMatrix();
    matrix = projectionViewMatrix * modelMatrix;

    glUniformMatrix4fv(textureUniforms.u_modelMatrix, 1, GL_FALSE, (GLfloat*) &modelMatrix);
    glUniformMatrix4fv(textureUniforms.u_normalMatrix, 1, GL_FALSE, (GLfloat*) &normalMatrix);
    glUniformMatrix4fv(textureUniforms.u_matrix, 1, GL_FALSE, (GLfloat*) &matrix);

    starship.draw();

    // Draw the module
    for (TexturedModel& part : module) {
        modelMatrix = part.getModelMatrix();
        normalMatrix = part.getNormalMatrix();
        matrix = projectionViewMatrix * modelMatrix;

        glUniformMatrix4fv(textureUniforms.u_modelMatrix, 1, GL_FALSE, (GLfloat*) &modelMatrix);
        glUniformMatrix4fv(textureUniforms.u_normalMatrix, 1, GL_FALSE, (GLfloat*) &normalMatrix);
        glUniformMatrix4fv(textureUniforms.u_matrix, 1, GL_FALSE, (GLfloat*) &matrix);

        part.draw();
    }

    // Draw the planet
    modelMatrix = planet.getModelMatrix();
    normalMatrix = planet.getNormalMatrix();
    matrix = projectionViewMatrix * modelMatrix;

    glUniformMatrix4fv(textureUniforms.u_modelMatrix, 1, GL_FALSE, (GLfloat*) &modelMatrix);
    glUniformMatrix4fv(textureUniforms.u_normalMatrix, 1, GL_FALSE, (GLfloat*) &normalMatrix);
    glUniformMatrix4fv(textureUniforms.u_matrix, 1, GL_FALSE, (GLfloat*) &matrix);

    planet.draw();

    glUseProgram(blendingProgram);

    // Draw the surface
    glUniform1f(blendingUniforms.u_blendingFactor, surfaceBlendingFactor);

    glUniform3fv(blendingUniforms.u_cameraPosition, 1, (GLfloat*) &camera.position);

    glUniform3fv(blendingUniforms.u_fog.color, 1, (GLfloat*) &FOG_COLOR);
    glUniform1f(blendingUniforms.u_fog.maxEffect, fogMaxEffect);
    glUniform1f(blendingUniforms.u_fog.start, FOG_START);
    glUniform1f(blendingUniforms.u_fog.end, FOG_END);

    modelMatrix = surface.getModelMatrix();
    matrix = projectionViewMatrix * modelMatrix;

    glUniformMatrix4fv(blendingUniforms.u_modelMatrix, 1, GL_FALSE, (GLfloat*) &modelMatrix);
    glUniformMatrix4fv(blendingUniforms.u_matrix, 1, GL_FALSE, (GLfloat*) &matrix);

    surface.draw();

    glDisable(GL_DEPTH_TEST);
}
